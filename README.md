# Introduction
What's a Custom Editor? https://docs.unity3d.com/Manual/editor-CustomEditors.html
A Custom Editor is a script that lets you customize how fields of a certain class are presented in the inspector. The concept is sometimes refered as "Custom Inspector" as well.

# Description
With only one key shorcut, you may automatically create a Custom Editor script for a .cs file, ready to be edited and customized.
This does not customize the Inspector for you, but creates the required base code so you can customize it yourself.

This project consists of two main parts:
1. A python script that receives a *cs* file and extracts its variables to create the custom editor *cs* file.
2. A VSCode task that calls the python script and opens the new file with just pressing some key combination.

# How to use/install
You'll need to download some files and do some setup in VSCode. No actual installation required.
[Pendant]

# How it works
Python regex.
VSCode tasks.
[Pendant]

# Other links
Unity icon created by Freepik
https://www.flaticon.com/free-icons/unity
