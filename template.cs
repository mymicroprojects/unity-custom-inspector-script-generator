using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using EditorUtils;

[CustomEditor(typeof([[ClassType]]))]
public class [[ClassType]]Editor : Editor
{
    MyFields utils;

    #region Serialized Properties
[[SerializedProperties]]
    #endregion

    #region Foldout Bools
    #endregion

    public void OnEnable()
    {
        utils = new MyFields();

[[OnEnable]]
    }

    public override void OnInspectorGUI()
    {
        GUI.enabled = false;
        EditorGUILayout.ObjectField(
            "Script",
            MonoScript.FromMonoBehaviour(([[ClassType]])target),
            typeof([[ClassType]]),
            false
        );
        GUI.enabled = true;

[[OnInspectorGUI]]
        serializedObject.ApplyModifiedProperties();
    }

    private bool BeginFold(bool b, string text) => EditorGUILayout.BeginFoldoutHeaderGroup(b, text);
    private void EndFold() => EditorGUILayout.EndFoldoutHeaderGroup();
    
}