using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PlayerMovement))]
public class PlayerMovementEditor : Editor
{
    #region Serialized Properties
	SerializedProperty rbody;
	SerializedProperty animator;
	SerializedProperty spriteRenderer;
	SerializedProperty floorDetection;
	SerializedProperty spriteTrailPrefab;
	SerializedProperty jumpPower;
	SerializedProperty walkSpeed;
	SerializedProperty dashSpeed;
	SerializedProperty dashDuration;
	SerializedProperty dashTrailInterval;
	SerializedProperty canDashEffectSpeedMultiplier;
	SerializedProperty canDashEffectInterval;
	SerializedProperty dashTrailColor;
	SerializedProperty gravitySwitchTrailInterval;
	SerializedProperty gravitySwitchTrailColor;
	SerializedProperty canDash;

    #endregion

    #region Foldout Bools
    #endregion

    #region Constants
    private const float spaceBeforeHeader = 5;
    private const float spaceAfterHeader = 0;
    #endregion

    public void OnEnable()
    {
		rbody = serializedObject.FindProperty("rbody");
		animator = serializedObject.FindProperty("animator");
		spriteRenderer = serializedObject.FindProperty("spriteRenderer");
		floorDetection = serializedObject.FindProperty("floorDetection");
		spriteTrailPrefab = serializedObject.FindProperty("spriteTrailPrefab");
		jumpPower = serializedObject.FindProperty("jumpPower");
		walkSpeed = serializedObject.FindProperty("walkSpeed");
		dashSpeed = serializedObject.FindProperty("dashSpeed");
		dashDuration = serializedObject.FindProperty("dashDuration");
		dashTrailInterval = serializedObject.FindProperty("dashTrailInterval");
		canDashEffectSpeedMultiplier = serializedObject.FindProperty("canDashEffectSpeedMultiplier");
		canDashEffectInterval = serializedObject.FindProperty("canDashEffectInterval");
		dashTrailColor = serializedObject.FindProperty("dashTrailColor");
		gravitySwitchTrailInterval = serializedObject.FindProperty("gravitySwitchTrailInterval");
		gravitySwitchTrailColor = serializedObject.FindProperty("gravitySwitchTrailColor");
		canDash = serializedObject.FindProperty("canDash");

        SetupHorizontalLine(horizontalLine);
    }

    public override void OnInspectorGUI()
    {
		EditorGUILayout.PropertyField(rbody);
		EditorGUILayout.PropertyField(animator);
		EditorGUILayout.PropertyField(spriteRenderer);
		EditorGUILayout.PropertyField(floorDetection);
		EditorGUILayout.PropertyField(spriteTrailPrefab);
		EditorGUILayout.PropertyField(jumpPower);
		EditorGUILayout.PropertyField(walkSpeed);
		EditorGUILayout.PropertyField(dashSpeed);
		EditorGUILayout.PropertyField(dashDuration);
		EditorGUILayout.PropertyField(dashTrailInterval);
		EditorGUILayout.PropertyField(canDashEffectSpeedMultiplier);
		EditorGUILayout.PropertyField(canDashEffectInterval);
		EditorGUILayout.PropertyField(dashTrailColor);
		EditorGUILayout.PropertyField(gravitySwitchTrailInterval);
		EditorGUILayout.PropertyField(gravitySwitchTrailColor);
		EditorGUILayout.PropertyField(canDash);

        serializedObject.ApplyModifiedProperties();
    }

    private void MyHeader(string text ,float spaceBeforeHeader = spaceBeforeHeader, float spaceAfterHeader = spaceAfterHeader) {
        EditorGUILayout.Space(spaceBeforeHeader);
        EditorGUILayout.LabelField(text, EditorStyles.boldLabel);
        EditorGUILayout.Space(spaceAfterHeader);
    }

    private bool BeginFold(bool b, string text) => EditorGUILayout.BeginFoldoutHeaderGroup(b, text);
    private void EndFold() => EditorGUILayout.EndFoldoutHeaderGroup();

    private void SetupHorizontalLine(GUIStyle horizontalLine) {
        horizontalLine = new GUIStyle();
        horizontalLine.normal.background = EditorGUIUtility.whiteTexture;
        horizontalLine.margin = new RectOffset(0, 0, 6, 4);
        horizontalLine.fixedHeight = 1;
    }

    private void HorizontalLine(Color color) {
        Color originalGUIColor = GUI.color;
        GUI.color = color;
        GUILayout.Box(GUIContent.none, horizontalLine);
        GUI.color = originalGUIColor;
    }
    
}