import re
import sys
import os

# Processing Command Line Arguments and reading files --------------------------------------------------------------------
filePath = sys.argv[1]
print("\n\nFile path:\n" + filePath + "\n\n")
sourceCode = open(filePath, "r").read()

classType = sys.argv[2]
currentFolder = sys.argv[3]
projectPath = sys.argv[4]

editorFolderPath = projectPath + "\\Assets\\Scripts\\Editor"
editorFolderName = editorFolderPath + "\\EditorUtils.cs"
print("Project path: " + editorFolderPath)

originalTemplate = open(sys.path[0] + "\\template.cs").read()
editorUtilsTemplate = open(sys.path[0] + "\\editorUtilitiesTemplate.cs").read()

# Attributes declaration -------------------------------------------------------------------------------------------------
serializedProperties = ""
onEnable = ""
onInspectorGUI = ""

# Regex processing and output generation ---------------------------------------------------------------------------------
variables = re.finditer(" *(?P<inspector>\\[(?:SerializeField|HideInInspector)\\] +)?" +
                        "(?P<access>public|private|protected) +" +
                        "(?P<type>[\\w<>_]+) +" +
                        "(?P<name>[\\w_]+) *" +
                        "(?P<getset>{(?: *(?:private)? +get *;)? *(?: *(?:private)? +set *;)? *} *)?" +
                        "(?:= *(?P<init>[\\w<>_\\(\\)]+))?" +
                        ";\\n", sourceCode)

# Methods are not used, but I'll leave the regex here
"""methods = re.finditer(" *(?P<access>public|private|protected) +" +
                      "(?P<implementation>override|virtual|abstract)?" +
                      "(?P<type>[\\w<>_]+) +" +
                      "(?P<name>[\\w_]+) *" +
                      "\\((?P<args>(?:[\\w_, ]+)+)?\\)" +
                      " *[{;]\\n", sourceCode)"""

print("Variables to include in the editor:")
for variable in variables:
    properties = variable.groupdict()
    print(str(properties["inspector"]) + "\t" + properties["access"] + "\t" + properties["name"], end="")
    varName = properties["name"]

    if str(properties["inspector"]) == "[HideInInspector]":
        print("\tFALSE", end=" \n")
        continue

    if str(properties["access"]) == "private" and str(properties["inspector"]) != "[SerializeField]":
        print("\tFALSE", end=" \n")
        continue

    print("\tTRUE", end=" \n")
    serializedProperties = serializedProperties + "\tSerializedProperty " + varName + ";" + "\n"
    onEnable = onEnable + "\t\t" + varName + " = serializedObject.FindProperty(\"" + varName + "\");" + "\n"
    onInspectorGUI = onInspectorGUI + "\t\tEditorGUILayout.PropertyField(" + varName + ");" + "\n"

# Not used
"""print("\nMethods:")
for method in methods:
    print(method.groupdict())"""


# Replace attributes in template -----------------------------------------------------------------------------------------
attributes = {
    "[[ClassType]]": classType,
    "[[SerializedProperties]]": serializedProperties,
    "[[OnEnable]]": onEnable,
    "[[OnInspectorGUI]]": onInspectorGUI
}
template = originalTemplate

for attribute in attributes:
    value = attributes[attribute]
    template = template.replace(attribute, value)

# Creating the EditorUtilities file if it does not exist yet -------------------------------------------------------------
try:
    utilsFile = open(editorFolderName, "x")
    utilsFile.write(editorUtilsTemplate)
    print("EditorUtilities.cs created!")
except FileExistsError:
    print("EditorUtilities.cs already exists!")

# Output file ----------------------------------------------------------------------------------------------------------
# Local alternative for testing
#exportFile = classType + "Editor.cs"

# Path for the file to be exported to
# If you change this, you must do some changes to the tasks.json file as well
exportFile = editorFolderPath + "\\" + classType + "Editor.cs"

f = open(exportFile, "w")
f.write(template)
print("Editor script file for the class generated successfully!")
print()